# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4. 
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201) 
# NOTE: Feel free to add any extra member variables/functions you like. 

class RangeList
  attr_accessor :range_list

  def initialize
    self.range_list = []
  end

  def add(range)
    return if before_add_handle(range)
    temp = []
    merged = false
    self.range_list.each do |item|
      unless merged
        if range.first > item.last
          temp << item
        else
          if range.last < item.first
            temp << range
            temp << item
          else
            temp << [[range.first, item.first].min, [range.last, item.last].max]  
          end
          merged = true
        end
      else
        if item.first <= temp.last.last
          temp[-1] = [[item.first, temp[-1].first].min, [item.last, temp[-1].last].max]  
        else
          temp << item
        end
      end
    end
    self.range_list = temp
  end

  def remove(range)
    return if before_remove_handle(range)
    temp = []
    self.range_list.each do |item|
      if range.last <= item.first || item.last <= range.first
        temp << item
      elsif range.first <= item.first && range.last >= item.last
        next
      else
        sub_ranges = split_range(range, item)
        temp.push(*sub_ranges)
      end
    end
    self.range_list = temp
  end

  def print
    self.range_list.map{|range| "[#{range.join(', ')})"}.join(' ')
  end

  private

  def min
    return self.range_list.first.first
  end

  def max
    return self.range_list.last.last
  end

  def before_add_handle(range)
    l, r = range

    return true if l >= r

    if self.range_list.size == 0 || l > max
      self.range_list << range
      return true
    end

    if r < min
      self.range_list.unshift range
      return true
    end

    if r == min
      self.range_list.first[0] = l
      return true
    end

    if l == max
      self.range_list.last[1] = r
      return true
    end

    return false
  end

  def before_remove_handle(range)
    l, r = range
    return true if l >= r
    return true if self.range_list.size == 0
    return true if r <= min
    return true if l >= max
    return false
  end

  def split_range(r_range, o_range)
    sub_ranges = [
      [o_range.first, r_range.first],
      [r_range.last, o_range.last]
    ]
    return sub_ranges.select{|range| range.last > range.first}
  end
end