require 'rspec'
require '../range_list'

RSpec.describe 'RangeList', :obj => RangeList.new do
  context 'add' do
    it 'Should display: [1, 5)' do |example|
      example.metadata[:obj].add([1, 5])
      expect(example.metadata[:obj].print).to eq('[1, 5)')
    end

    it 'Should display: [1, 5) [10, 20)' do |example|
      example.metadata[:obj].add([10, 20])
      expect(example.metadata[:obj].print).to eq('[1, 5) [10, 20)')
    end

    it 'Should display: [1, 5) [10, 20)' do |example|
      example.metadata[:obj].add([20, 20])
      expect(example.metadata[:obj].print).to eq('[1, 5) [10, 20)')
    end

    it 'Should display: [1, 5) [10, 21)' do |example|
      example.metadata[:obj].add([20, 21])
      expect(example.metadata[:obj].print).to eq('[1, 5) [10, 21)')
    end

    it 'Should display: [1, 5) [10, 21)' do |example|
      example.metadata[:obj].add([2, 4])
      expect(example.metadata[:obj].print).to eq('[1, 5) [10, 21)')
    end

    it 'Should display: [1, 8) [10, 21)' do |example|
      example.metadata[:obj].add([3, 8])
      expect(example.metadata[:obj].print).to eq('[1, 8) [10, 21)')
    end
  end

  context 'remove' do
    it 'Should display: [1, 8) [10, 21)' do |example|
      example.metadata[:obj].remove([10, 10])
      expect(example.metadata[:obj].print).to eq('[1, 8) [10, 21)')
    end

    it 'Should display: [1, 8) [11, 21)' do |example|
      example.metadata[:obj].remove([10, 11])
      expect(example.metadata[:obj].print).to eq('[1, 8) [11, 21)')
    end

    it 'Should display: [1, 8) [11, 15) [17, 21)' do |example|
      example.metadata[:obj].remove([15, 17])
      expect(example.metadata[:obj].print).to eq('[1, 8) [11, 15) [17, 21)')
    end

    it 'Should display: [1, 3) [19, 21)' do |example|
      example.metadata[:obj].remove([3, 19])
      expect(example.metadata[:obj].print).to eq('[1, 3) [19, 21)')
    end
  end
end